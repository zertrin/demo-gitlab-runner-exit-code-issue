#!/bin/bash
set -euo pipefail
set -x

main() {
    rc=0 # initialize return code

    # in real script, the subshell is replaced by the actual command to test
    ( echo "Run test..." | tee -a test.log; exit 66 ) || rc=$?

    echo -e "\nReturn code from cmd: ${rc}\n" | tee -a test.log

    if [[ ${rc} != 0 ]]; then
        exit ${rc}
    fi
}

main
exit 0
